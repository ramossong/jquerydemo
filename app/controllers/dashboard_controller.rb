class DashboardController < ApplicationController
  def index
    @totalSalesItems=SalesItem.count
    @totalPrice=0
    items=SalesItem.all
    items.each do |item|
      @totalPrice=@totalPrice+item.price
    end

    @avgSalesPrice=@totalPrice/@totalSalesItems
  end

end
