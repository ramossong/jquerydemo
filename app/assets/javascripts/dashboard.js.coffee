# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


updateNotes = ->
  selection_id = $('#sales_item_category_id').val()
  $.getJSON '/categories/' + selection_id + '/notes', {},(json, response) ->
    $('#notes').text json['notes']

$ ->
  $(document).ready ->
    $("#average").mouseover(->
      $("#description").fadeIn());
  $(document).ready ->
    $("#average").mouseout(->
      $("#description").fadeOut());